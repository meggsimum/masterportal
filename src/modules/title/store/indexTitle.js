import getters from "./gettersTitle";

export default {
    namespaced: true,
    getters
};
