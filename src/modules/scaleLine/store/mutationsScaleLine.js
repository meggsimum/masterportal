export default {
    updateScaleNumber (state, scaleNumber) {
        state.scaleNumber = scaleNumber;
    },
    updateScaleLineValue (state, scaleLineValue) {
        state.scaleLineValue = scaleLineValue;
    }
};
