
/**
 * @returns {boolean} true if screen is considered mobile device
 */
function isMobile () {
    return window.innerWidth < 768;
}

export default isMobile;
