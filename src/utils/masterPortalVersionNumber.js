import packageJson from "../../package.json";

/**
 * @constant {string} masterPortalVersionNumber masterportal version as noted in package.json
 */
const masterPortalVersionNumber = packageJson.version;

export default masterPortalVersionNumber;
